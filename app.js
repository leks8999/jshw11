const icon = document.querySelectorAll('i')
const btn = document.querySelector('button')
const form = document.forms[0]
const inputTop = document.getElementsByTagName('input')[0]
const inputBottom = document.getElementsByTagName('input')[1]
const erortxt = document.createElement('p')
const bottomLabel = document.querySelectorAll('label')[1]

form.addEventListener('click', (e) => {
    e.preventDefault()
    icon.forEach(item => {
        if (e.target === item) {
            item.classList.toggle('fa-eye-slash')
            if (item.previousElementSibling.getAttribute('type') === 'password') {
                item.previousElementSibling.setAttribute('type', 'text')
            } else { item.previousElementSibling.setAttribute('type', 'password') }
        }
    })
    if (e.target === btn) {
        if (inputTop.value === inputBottom.value) {
            alert('You are welcome')
            erortxt.remove()
        } else {
            erortxt.textContent = 'Потрібно ввести однакові значення'
            erortxt.style.color = 'red'
            bottomLabel.append(erortxt)
        }
    }
})

